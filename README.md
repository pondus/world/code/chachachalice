ChaChaChaLice
=============

Micro samples to get started with Chalice.

1. Plain: Render plain text---yes, not very useful, but to see the mechanics: A Chalice Reponse wraps a character string.
2. In-memory templates: Use of the Jinja2 common template engine to generate HTML stings from a template specified in a Python variable.
3. File templating: The common way of using Jinja2 (basically what is done transparently by `render_tempate` in Flask), with templates defined in HTML files, searched by default under `templates`.

Setup
-----

For each project, there is a requirements file:

    pip install -r requirements.txt

All assume Python3, and recommending to use some virtual environment wrapper.

Execution
---------

Under the folder of each project:

    chalice local

This creates a [local web server](http://localhost:8000) for viewing the results. Much less interesting than the code :-)
