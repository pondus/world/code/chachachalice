from chalice import Chalice, Response

app = Chalice(app_name='plain')

@app.route('/')
def index():
    return Response(body='hello world!',
                    status_code=200,
                    headers={'Content-Type': 'text/html'})
