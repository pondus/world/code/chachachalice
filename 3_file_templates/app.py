from chalice import Chalice, Response
from jinja2 import Environment, PackageLoader, select_autoescape

env = Environment(
    loader=PackageLoader("app"),  # For Chalice, I chose `app`, the same name as this file. Explained here: https://jinja.palletsprojects.com/en/3.1.x/api/#basics
    autoescape=select_autoescape()
)

app = Chalice(app_name='file_template')


@app.route('/')
def index():
    template = env.get_template("hello.html")
    return Response(body=template.render(name="world"),
                    status_code=200,
                    headers={'Content-Type': 'text/html'})
