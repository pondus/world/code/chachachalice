from chalice import Chalice, Response
from jinja2 import Template

app = Chalice(app_name='in_memory_template')

@app.route('/')
def index():
    template = Template("<h1>Hello {{ name }}</h1>")
    return Response(body=template.render(name="world"),
                    status_code=200,
                    headers={'Content-Type': 'text/html'})
